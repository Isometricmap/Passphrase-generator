# Passphrase Generator
  - A super simple passphrase generator in python. Uses the `secrets` module to produce a passphrase of random words chosen from a list of ~100k English words, taken from `/usr/share/dict/words` in Ubuntu.

  Only compatible with `python3.6`

# Example Usage

  - Sample input

  `python3 password_gen.py 20 25`

 - Sample output
   `welder
    bark's
    investitures`
